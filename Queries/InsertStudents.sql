SET IDENTITY_INSERT dbo.Students ON
DELETE FROM dbo.Students
INSERT INTO dbo.Students
(
 [ID], [FirstName], [LastName], [ClassID], [Interest]
)
VALUES
(
 1, 'Student0', 'LastName0', 0, 0
),
(
 2, 'Student1', 'LastName1', 0, 1
),
(
 3, 'Student2', 'LastName2', 0, 2
),
(
 4, 'Student3', 'LastName3', 0, 3
),
(
 5, 'Student4', 'LastName4', 0, 4
),
(
 6, 'Student5', 'LastName5', 1, 0
),
(
 7, 'Student6', 'LastName6', 1, 1
),
(
 8, 'Student7', 'LastName7', 1, 2
),
(
 9, 'Student8', 'LastName8', 1, 3
),
(
 10, 'Student9', 'LastName9', 1, 4
),
(
 11, 'Student10', 'LastName10', 2, 0
),
(
 12, 'Student11', 'LastName11', 2, 1
),
(
 13, 'Student12', 'LastName12', 2, 2
),
(
 14, 'Student13', 'LastName13', 2, 3
),
(
 15, 'Student14', 'LastName14', 2, 4
),
(
 16, 'Student15', 'LastName15', 3, 0
),
(
 17, 'Student16', 'LastName16', 3, 1
),
(
 18, 'Student17', 'LastName17', 3, 2
),
(
 19, 'Student18', 'LastName18', 3, 3
),
(
 20, 'Student19', 'LastName19', 3, 4
)
GO

SELECT * FROM dbo.Students
GO