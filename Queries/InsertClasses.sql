SET IDENTITY_INSERT dbo.Classes ON
DELETE FROM dbo.Classes
INSERT INTO dbo.Classes
(
 [ID], [Name]
)
VALUES
(
 1, 'Class0'
),
(
 2, 'Class1'
),
(
 3, 'Class2'
),
(
 4, 'Class3'
)
GO

SELECT * FROM dbo.Classes
GO