﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudentsInterestsData;

namespace StudentsInterestsServices
{
    public class QueryService
    {
        public QueryService()
        {
            // Connection check
            try
            {
                List<Student> Students = _context.Students.ToList();
            }
            catch (System.Exception)
            {
                Console.WriteLine("The database is not connected");
                Environment.Exit(0);
            }
        }

        private readonly StudentsInterestsContext _context = new();

        // get a list of students grouped by a specific interest in the school
        public Dictionary<StudentInterest, List<Student>> GroupByInterests()
        {
            return _context.Students
                .ToList()
                .GroupBy(student => student.Interest)
                .ToDictionary(group => group.Key, group => group.ToList());
        }

        // get a list of students grouped by a specific interest in the class
        public Dictionary<StudentInterest, List<Student>> GroupByInterestsInClass(int classID)
        {
            return _context.Students
                .Where(Student => Student.ClassID == classID)
                .ToList()
                .GroupBy(student => student.Interest)
                .ToDictionary(group => group.Key, group => group.ToList());
        }

        // get a list of students with the specific interest in the school
        public List<Student> GetByInterest(string interestString)
        {
            StudentInterest interest;
            if (Enum.TryParse<StudentInterest>(interestString, true, out interest))
                return _context.Students
                    .Where(student => student.Interest == interest)
                    .ToList();

            return new List<Student>();
        }

        // get a list count of students by a specific interest
        public Dictionary<StudentInterest, int> CountAllByInterests()
        {
            return _context.Students
                .ToList()
                .GroupBy(student => student.Interest)
                .ToDictionary(group => group.Key, group => group.Count());
        }

        // count students with a specific interest
        public int CountByInterest(string interestString)
        {
            StudentInterest interest;
            if (Enum.TryParse<StudentInterest>(interestString, true, out interest))
                return _context.Students
                    .Where(s => s.Interest == interest)
                    .Count();
            return 0;
        }

        // search for students with specific interest in the specific class
        public List<Student> GetByInterestAndClass(string interestString, int classID)
        {
            StudentInterest interest;
            if (Enum.TryParse<StudentInterest>(interestString, true, out interest))
                return _context.Students
                            .Where(student => student.Interest == interest && student.ClassID == classID)
                            .ToList();
            return new List<Student>();
        }

        // get calculate average count of students with specific interest in classes
        public double AverageCountByInterest(string interestString)
        {
            StudentInterest interest;

            if (!Enum.TryParse<StudentInterest>(interestString, true, out interest))
                return 0;

            return _context.Students
                    .GroupBy(student => student.ClassID)
                    .Select(group => group
                                        .Where(student => student.Interest == interest)
                                        .Count()
                    )
                    .Average();
        }
    }
}
