# Students Interests

The is test assignment for Microsoft Entity Framework course.

1. Add a database with the following tables:
- Students (ID, First Name, Last Name, Class ID, Interest). Interest should be an enum - math, literature, foreign languages, etc
- Classes (ID, Name)

2. Create a new project - StudentsInterest.Data :
- add Entity Framework reference to the project
- add Entity Framework DataContext (use code first approach) to it

3. Create a new project - StudentsInterest.Services :
- add logic that will get a list of students grouped by a specific interest in the school
- add logic that will get a list of students grouped by a specific interest in the class
- add logic that will get a list count of students by a specific interest
- add logic that will search for students with specific interest in the specific class
- add logic that will get calculate average count of students with specific interest in classes

4. Write unit tests for logic above - call it StudentsInterest.Tests