using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentsInterestsServices;
using StudentsInterestsData;
using System.Collections.Generic;
using System.Linq;

namespace StudentsInterestsTests
{
    [TestClass]
    public class QueryServiceTest
    {
        QueryService queryService = new();

        [TestMethod]
        public void GroupByInterests()
        {
            var expectedResult = new Dictionary<StudentInterest, List<string>>()
            {
                { StudentInterest.Math, new List<string> { "Student0", "Student5", "Student10", "Student15" } },
                { StudentInterest.Literature, new List<string> { "Student1", "Student6", "Student11", "Student16" } },
                { StudentInterest.English, new List<string> { "Student2", "Student7", "Student12", "Student17" } },
                { StudentInterest.Music, new List<string> { "Student3", "Student8", "Student13", "Student18" } },
                { StudentInterest.History, new List<string> { "Student4", "Student9", "Student14", "Student19" } }
            };

            Dictionary<StudentInterest, List<string>> actualResult = queryService
                .GroupByInterests()
                .ToDictionary(pair => pair.Key, pair => pair.Value.Select(student => student.FirstName).ToList());


            bool result = expectedResult.All(pair => pair.Value.SequenceEqual(actualResult[pair.Key]));
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GroupByInterestsInClass()
        {
            var expectedResult = new Dictionary<StudentInterest, List<string>>()
            {
                { StudentInterest.Math, new List<string> { "Student0" } },
                { StudentInterest.Literature, new List<string> { "Student1" } },
                { StudentInterest.English, new List<string> { "Student2" } },
                { StudentInterest.Music, new List<string> { "Student3" } },
                { StudentInterest.History, new List<string> { "Student4" } }
            };

            Dictionary<StudentInterest, List<string>> actualResult = queryService
                .GroupByInterestsInClass(0)
                .ToDictionary(pair => pair.Key, pair => pair.Value.Select(student => student.FirstName).ToList());


            bool resultBool = expectedResult.All(pair => pair.Value.SequenceEqual(actualResult[pair.Key]));
            Assert.IsTrue(resultBool);
        }

        [TestMethod]
        public void GetByInterest()
        {
            var expectedResult = new List<string>()
            {
                "Student2",
                "Student7",
                "Student12",
                "Student17"
            };

            List<string> actualResult = queryService
                    .GetByInterest("English")
                    .Select(Student => Student.FirstName)
                    .ToList();

            CollectionAssert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void CountAllByInterests()
        {
            var expectedResult = new Dictionary<StudentInterest, int>
            {
                { StudentInterest.Math, 4 },
                { StudentInterest.Literature, 4 },
                { StudentInterest.English, 4 },
                { StudentInterest.Music, 4 },
                { StudentInterest.History, 4}
            };

            Dictionary<StudentInterest, int> actualResult = queryService
                                                                .CountAllByInterests();

            bool resultBool = expectedResult.All(pair => pair.Value == actualResult[pair.Key]);
            Assert.IsTrue(resultBool);
        }

        [TestMethod]
        public void CountByInterest()
        {
            int expectedResult = 4;
            int actualResult = queryService.CountByInterest("math");
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void GetByInterestAndClass()
        {
            var expectedResult = new List<string>()
            {
                "Student16"
            };

            List<string> actualResult = queryService
                    .GetByInterestAndClass("LIterature", 3)
                    .Select(student => student.FirstName)
                    .ToList();

            CollectionAssert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void AverageCountByInterest()
        {
            double expectedResult = 1;

            double actualResult = queryService
                    .AverageCountByInterest("music");

            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
