﻿using Microsoft.EntityFrameworkCore;

namespace StudentsInterestsData
{
    public class StudentsInterestsContext : DbContext
    {
        public virtual DbSet<Student> Students { set; get; }
        public virtual DbSet<Class> Classes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "Server=localhost;Database=StudentsInterests;User=SA;Password=SqlPassword1;";
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            StudentsInterestsInitializer initializer = new(modelBuilder);
        }
    }

    public enum StudentInterest
    {
        Math,
        Literature,
        English,
        Music,
        History
    }

    public class Student
    {
        public int ID { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public int ClassID { set; get; }
        public StudentInterest Interest { set; get; }
    }

    public class Class
    {
        public int ID { set; get; }
        public string Name { set; get; }
    }
}
