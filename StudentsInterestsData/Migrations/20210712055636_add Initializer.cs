﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StudentsInterestsData.Migrations
{
    public partial class addInitializer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Classes",
                columns: new[] { "ID", "Name" },
                values: new object[,]
                {
                    { 1, "Class0" },
                    { 2, "Class1" },
                    { 3, "Class2" },
                    { 4, "Class3" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "ID", "ClassID", "FirstName", "Interest", "LastName" },
                values: new object[,]
                {
                    { 18, 3, "Student17", 2, "LastName17" },
                    { 17, 3, "Student16", 1, "LastName16" },
                    { 16, 3, "Student15", 0, "LastName15" },
                    { 15, 2, "Student14", 4, "LastName14" },
                    { 14, 2, "Student13", 3, "LastName13" },
                    { 13, 2, "Student12", 2, "LastName12" },
                    { 12, 2, "Student11", 1, "LastName11" },
                    { 11, 2, "Student10", 0, "LastName10" },
                    { 10, 1, "Student9", 4, "LastName9" },
                    { 8, 1, "Student7", 2, "LastName7" },
                    { 19, 3, "Student18", 3, "LastName18" },
                    { 7, 1, "Student6", 1, "LastName6" },
                    { 6, 1, "Student5", 0, "LastName5" },
                    { 5, 0, "Student4", 4, "LastName4" },
                    { 4, 0, "Student3", 3, "LastName3" },
                    { 3, 0, "Student2", 2, "LastName2" },
                    { 2, 0, "Student1", 1, "LastName1" },
                    { 1, 0, "Student0", 0, "LastName0" },
                    { 9, 1, "Student8", 3, "LastName8" },
                    { 20, 3, "Student19", 4, "LastName19" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "ID",
                keyValue: 20);
        }
    }
}
