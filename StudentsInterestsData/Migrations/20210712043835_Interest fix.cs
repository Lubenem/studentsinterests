﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StudentsInterestsData.Migrations
{
    public partial class Interestfix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Interst",
                table: "Students",
                newName: "Interest");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Interest",
                table: "Students",
                newName: "Interst");
        }
    }
}
