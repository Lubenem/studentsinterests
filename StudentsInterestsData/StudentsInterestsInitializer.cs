using System;
using Microsoft.EntityFrameworkCore;

namespace StudentsInterestsData
{
    public class StudentsInterestsInitializer
    {
        public StudentsInterestsInitializer(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().HasData(
                new Student { ID = 1, FirstName = "Student0", LastName = "LastName0", ClassID = 0, Interest = StudentInterest.Math },
                new Student { ID = 2, FirstName = "Student1", LastName = "LastName1", ClassID = 0, Interest = StudentInterest.Literature },
                new Student { ID = 3, FirstName = "Student2", LastName = "LastName2", ClassID = 0, Interest = StudentInterest.English },
                new Student { ID = 4, FirstName = "Student3", LastName = "LastName3", ClassID = 0, Interest = StudentInterest.Music },
                new Student { ID = 5, FirstName = "Student4", LastName = "LastName4", ClassID = 0, Interest = StudentInterest.History },

                new Student { ID = 6, FirstName = "Student5", LastName = "LastName5", ClassID = 1, Interest = StudentInterest.Math },
                new Student { ID = 7, FirstName = "Student6", LastName = "LastName6", ClassID = 1, Interest = StudentInterest.Literature },
                new Student { ID = 8, FirstName = "Student7", LastName = "LastName7", ClassID = 1, Interest = StudentInterest.English },
                new Student { ID = 9, FirstName = "Student8", LastName = "LastName8", ClassID = 1, Interest = StudentInterest.Music },
                new Student { ID = 10, FirstName = "Student9", LastName = "LastName9", ClassID = 1, Interest = StudentInterest.History },

                new Student { ID = 11, FirstName = "Student10", LastName = "LastName10", ClassID = 2, Interest = StudentInterest.Math },
                new Student { ID = 12, FirstName = "Student11", LastName = "LastName11", ClassID = 2, Interest = StudentInterest.Literature },
                new Student { ID = 13, FirstName = "Student12", LastName = "LastName12", ClassID = 2, Interest = StudentInterest.English },
                new Student { ID = 14, FirstName = "Student13", LastName = "LastName13", ClassID = 2, Interest = StudentInterest.Music },
                new Student { ID = 15, FirstName = "Student14", LastName = "LastName14", ClassID = 2, Interest = StudentInterest.History },

                new Student { ID = 16, FirstName = "Student15", LastName = "LastName15", ClassID = 3, Interest = StudentInterest.Math },
                new Student { ID = 17, FirstName = "Student16", LastName = "LastName16", ClassID = 3, Interest = StudentInterest.Literature },
                new Student { ID = 18, FirstName = "Student17", LastName = "LastName17", ClassID = 3, Interest = StudentInterest.English },
                new Student { ID = 19, FirstName = "Student18", LastName = "LastName18", ClassID = 3, Interest = StudentInterest.Music },
                new Student { ID = 20, FirstName = "Student19", LastName = "LastName19", ClassID = 3, Interest = StudentInterest.History });


            modelBuilder.Entity<Class>().HasData(
                    new Class { ID = 1, Name = "Class0" },
                    new Class { ID = 2, Name = "Class1" },
                    new Class { ID = 3, Name = "Class2" },
                    new Class { ID = 4, Name = "Class3" });
        }

    }
}
