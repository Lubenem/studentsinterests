﻿using System;
using StudentsInterestsServices;

namespace StudentsInterestsConsole
{
    class Program
    {
        static QueryService queryService = new();

        static void Main(string[] args)
        {
            Console.WriteLine(queryService.AverageCountByInterest("english"));
        }
    }
}
